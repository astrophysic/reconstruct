#!/bin/bash

declare -A dir_shower
dir_shower=( ["original"]="/home/hendrik/Showers" ["long"]="/home/hendrik/Showers/changed/long" ["short"]="/home/hendrik/Showers/changed/short")  

declare -A dir_rec
dir_rec=( ["original"]="../../Data/Reconstruct" ["long"]=".../../Data/Reconstruct/changed/long" ["short"]="../../Data/Reconstruct/short")  

for type in original  
do
for model in sibyll eposlhc qgsjet
do
for mass in proton
do
for energy in e19_00  
do
echo "${dir_rec[${type}]} $type ${model} ${mass} ${energy} t60 ..."

./Detection ${dir_rec[${type}]} ${model} ${mass} ${energy} t60 ${dir_shower[${type}]} > ${type}-${model}-${mass}-${energy}-t60.out

mv ../../Data/ReconstructModel/n19.hist ../../Data/ReconstructModel/n19-${type}-${model}-${mass}-${energy}-t60.hist 
mv ../../Data/ReconstructModel/nmu.hist ../../Data/ReconstructModel/nmu-${type}-${model}-${mass}-${energy}-t60.hist 
mv ../../Data/ReconstructModel/t0.hist ../../Data/ReconstructModel/t0-${type}-${model}-${mass}-${energy}-t60.hist 
mv ../../Data/ReconstructModel/th.hist ../../Data/ReconstructModel/th-${type}-${model}-${mass}-${energy}-t60.hist 
mv ../../Data/ReconstructModel/ph.hist ../../Data/ReconstructModel/ph-${type}-${model}-${mass}-${energy}-t60.hist 
mv ../../Data/ReconstructModel/rc.hist ../../Data/ReconstructModel/rc-${type}-${model}-${mass}-${energy}-t60.hist

done
done
done
done
