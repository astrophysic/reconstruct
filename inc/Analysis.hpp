#ifndef _ANALYSIS_HPP_
#define _ANALYSIS_HPP_

#include <iostream>
#include <string>

class Analysis 
{
private:
    std::string fileName;
    double nmu;
public:
    Analysis(std::string fileName);
    void AnalysisNmu();
    double GetNmu();
};

#endif /* _ANALYSIS_HPP_ */