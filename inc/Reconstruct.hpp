#ifndef _RECONSTRUCT_HPP_
#define _RECONSTRUCT_HPP_

#include <iostream>

#include "TFile.h"
#include "TH2F.h"
#include "TMinuit.h"
#include "Array.hpp"


/**
 * Classe para reconstrução
 */

class Reconstruct : public TObject
{
private:
    /** Fit results */
    double nmu, nmuError;
    double theta, thetaError;
    double phi, phiError;
    double rc, rcError;
    double t0, t0Error;
    /** Fit inputs */
    double* signal;
    double* error;
    double* posX;
    double* posY;
    double* time;
    TH2F* muonMap;
    size_t nDetectors;
public:
    Reconstruct();
    void SetArray(Array& array);
    void ClearArray();
    virtual ~Reconstruct();
    void ReconstructAll();
    void ReconstructNmu();
    void ReconstructAng();
    size_t GetNDetectors();
    double GetPosX(size_t i);
    double GetPosY(size_t i);
    double GetSignal(size_t i);
    double GetTime(size_t i);
    double GetMuonMap(double x, double y);

    double GetNmu();
    double GetT0();
    double GetTheta();
    double GetPhi();
    double GetRc();
};


/**
 * Funções auxiliares de minimização
 */

void MaximumLikelihood_N19 (Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag);

void ChiSquareAng (Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag);

#endif /* _RECONSTRUCT_HPP_ */