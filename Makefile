SRCDIR := src
OBJDIR := lib
INCDIR := inc

SRCS := $(wildcard $(SRCDIR)/*cpp)
OBJS := $(addprefix $(OBJDIR)/, $(notdir $(SRCS:.cpp=.o)))

INC := -Iinc -I../corsika/inc -I../dethinning/inc -I../histograms/inc -I../surface_array/inc
LIB := -L../corsika/lib -L../dethinning/lib -L../histograms/lib -L../surface_array/lib
CFLAGS := -g -std=c++17 -fopenmp
CLIBS := -lArray -lCorsika -lMinuit -lHistogram

CC := g++

all:libCorsika libDethinning libHist libArray Reconstruct

Reconstruct: $(OBJS) 
	$(CC) $(CFLAGS) -o $@ $^ $(LIB) $(CLIBS) `$(ROOTSYS)/bin/root-config --cflags --glibs`

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp 
	$(CC) $(CFLAGS) -c $< -o $@ $(INC) `$(ROOTSYS)/bin/root-config --cflags --glibs`

libCorsika:
	$(MAKE) -C ../corsika

libDethinning:
	$(MAKE) -C ../dethinning

libHist:
	$(MAKE) -C ../histograms

libArray:
	$(MAKE) -C ../surface_array

clean:
	rm -rf lib/*.o
	rm -rf lib/*.a
	$(MAKE) -C ../surface_array clean
	$(MAKE) -C ../histograms clean
	$(MAKE) -C ../dethinning clean
	$(MAKE) -C ../corsika clean
