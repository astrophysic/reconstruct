#include "Reconstruct.hpp"


Reconstruct::Reconstruct()
{
    /** Read reference muon map */
    TFile* rootFile = new TFile("map/EPOSLHC-19_00-P-T60-DeThinn.root");
    this->muonMap = (TH2F*) rootFile->Get("Muon2D");
    this->muonMap->SetDirectory(0);
    rootFile->Close();

}

void Reconstruct::SetArray(Array& array)
{
    /** Read array signal **/
    this->nDetectors = array.GetSize();
    this->posX = new double[this->nDetectors];
    this->posY = new double[this->nDetectors];
    this->signal = new double[this->nDetectors];
    this->error = new double[this->nDetectors];
    this->time = new double[this->nDetectors];

    array.GetData(this->posX, this->posY, this->signal, this->error, this->time);
}

void Reconstruct::ClearArray()
{
    if (this->nDetectors != 0)
    {
        delete this->signal;
        delete this->error;
        delete this->posX;
        delete this->posY;
        delete this->time;
    }
}

Reconstruct::~Reconstruct()
{
    delete this->muonMap;
    delete this->signal;
    delete this->error;
    delete this->posX;
    delete this->posY;
    delete this->time;
}

void Reconstruct::ReconstructAng()
{
    /** Number of free parameters */
    size_t nPar = 4;

    /** Init TMinuit */
    TMinuit *gMinuit = new TMinuit(nPar);
    gMinuit->SetObjectFit(this);
	gMinuit->SetFCN(ChiSquareAng);
	gMinuit->Command("SET PRINT -1");

        /** Initial parameters*/
    double parameters[nPar];
    double stepSize[nPar];
    double minValue[nPar];           
    double maxValue[nPar];           
    std::string parName[nPar]; 

    parameters[0] = 740000;
    stepSize[0] = 1;
    minValue[0] = 0;
    maxValue[0] = 0;
    parName[0] = "T_{0}"; 

    parameters[1] = 1.0;
    stepSize[1] = 0.001;
    minValue[1] = -10;
    maxValue[1] = 10;
    parName[1] = "u"; 

    parameters[2] = 1.0;
    stepSize[2] = 0.001;
    minValue[2] = -10;
    maxValue[2] = 10;
    parName[2] = "v"; 

    parameters[3] = 1/(10000*2*0.3);
    stepSize[3] = 0.001;
    minValue[3] = 0;
    maxValue[3] = 0;
    parName[3] = "R_{c}"; 

     for (size_t i=0; i<nPar; i++){
        gMinuit->DefineParameter(i, parName[i].c_str(),
        parameters[i], stepSize[i], minValue[i], maxValue[i]);
	}

    /** Execution flags */

    Double_t arglist[10];
	Int_t ierflg = 0;
	arglist[0] = 1000;
	arglist[1] = 1.;

    /** Minimization */
    gMinuit->mnexcm("MINIMIZE", arglist, 2, ierflg);
    gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);
    gMinuit->mnexcm("MINOS", arglist, 0, ierflg);

    double tr, trError;
    double ur, urError;
    double vr, vrError;
    double rr, rrError;

    gMinuit->GetParameter(0,tr,trError);
    gMinuit->GetParameter(1,ur,urError);
    gMinuit->GetParameter(2,vr,vrError);
    gMinuit->GetParameter(3,rr,rrError);

    double us, vs, rs;

    us = 1*ur*0.3;
    vs = 1*vr*0.3;
    rs = 1*rr*0.3;

    this->t0 = tr;
    this->theta = 180*asin(sqrt(us*us+vs*vs))/M_PI;
    this->phi = 180*atan2(vs,us)/M_PI;
    this->rc = 1.0/(2*rs);

    delete gMinuit;
    
}

void Reconstruct::ReconstructNmu()
{
    /** Number of free parameters */
    size_t nPar = 1;

    /** Init TMinuit */
    TMinuit *gMinuit = new TMinuit(nPar);
    gMinuit->SetObjectFit(this);
	gMinuit->SetFCN(MaximumLikelihood_N19);
	gMinuit->Command("SET PRINT -1");

    /** Initial parameters*/
    double parameters[nPar];
    double stepSize[nPar];
    double minValue[nPar];           
    double maxValue[nPar];           
    std::string parName[nPar]; 

    parameters[0] = 1.0;
    stepSize[0] = 0.01;
    minValue[0] = 0;
    maxValue[0] = 5;
    parName[0] = "N19"; 

    for (size_t i=0; i<nPar; i++){
        gMinuit->DefineParameter(i, parName[i].c_str(),
        parameters[i], stepSize[i], minValue[i], maxValue[i]);
	}

    /** Execution flags */

    Double_t arglist[10];
	Int_t ierflg = 0;
	arglist[0] = 1000;
	arglist[1] = 1.;

    /** Minimization */
    gMinuit->mnexcm("MINIMIZE", arglist, 2, ierflg);
    gMinuit->mnexcm("MIGRAD", arglist, 2, ierflg);
    gMinuit->mnexcm("MINOS", arglist, 0, ierflg);

    gMinuit->GetParameter(0,nmu,nmuError);

    delete gMinuit;
}

void ChiSquareAng (Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
    Reconstruct* object = (Reconstruct*) gMinuit->GetObjectFit();
    double chisq = 0;
    double sigt=25.*25.;
    double dt;

    for (int i=0; i<object->GetNDetectors(); i++){
        double x = object->GetPosX(i);
        double y = object->GetPosY(i);
        double signal = object->GetSignal(i);
        double time = object->GetTime(i);
        if (signal > 0){      
            double rp = par[1]*0.3*x + par[2]*0.3*y;
            double rho = sqrt(x*x+y*y);
            double delta = (rho*rho - rp*rp);

      dt =  time - (par[0] + par[1]*x + par[2]*y + delta*par[3]);
     }
     chisq += dt*dt/sigt;
   }
   f = chisq;
}

void MaximumLikelihood_N19 (Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
    Reconstruct* object = (Reconstruct*) gMinuit->GetObjectFit();

    double log_ll = 0;
    double lambda;
	for (size_t i = 0; i < object->GetNDetectors(); i++){
        double x = object->GetPosX(i);
        double y = object->GetPosY(i);
        double rho = object->GetMuonMap(x,y);
        double signal = object->GetSignal(i);
		lambda = par[0]*(M_PI*1.8*1.8)*rho;
		log_ll += -signal*log(lambda)+lambda;
	}
		f=log_ll;
}
    size_t Reconstruct::GetNDetectors()
    {
        return this->nDetectors;
    }

    double Reconstruct::GetPosX(size_t i)
    {
        return this->posX[i];
    }
    double Reconstruct::GetPosY(size_t i)
    {
        return this->posY[i];
    }
    double Reconstruct::GetSignal(size_t i)
    {
        return this->signal[i];
    }
    double Reconstruct::GetTime(size_t i)
    {
        return this->time[i];
    }
    double Reconstruct::GetMuonMap(double x, double y)
    {
        return this->muonMap->Interpolate(x,y);
    }
    double Reconstruct::GetNmu()
    {
        return this->nmu;
    }
    double Reconstruct::GetT0()
    {
        return this->t0;
    }
    double Reconstruct::GetTheta()
    {
        return this->theta;
    }
    double Reconstruct::GetPhi()
    {
        return this->phi;
    }
    double Reconstruct::GetRc()
    {
        return this->rc;
    }