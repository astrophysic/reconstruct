#include <iostream>
#include <string>
#include <iomanip>

#include "Array.hpp"
#include "Dethinning.hpp"
#include "Reconstruct.hpp"
#include "Analysis.hpp"
#include "H1D.hpp"

inline bool exists_file (const std::string& name);
std::string shower_name (size_t i);

main(int argc, char const *argv[])
{
    H1D hist_n19(150,0,5);
    H1D hist_nmu(150,0,5);
    H1D diff_nmu(150,-1,1);
    H1D hist_t0(150,736000,746000);
    H1D hist_th(50,50,70);
    H1D hist_ph(50,-10,10);
    H1D hist_rc(50,5000,15000);


    std::string file_dir =      std::string(argv[1])+"/"+ // directory
                                std::string(argv[2])+"/"+ // model
                                std::string(argv[3])+"/"+ // mass
                                std::string(argv[4])+"/"+ // energy
                                std::string(argv[5])+"/"; // ang

    std::string shower_dir =      std::string(argv[6])+"/"+ // directory
                                std::string(argv[2])+"/"+ // model
                                std::string(argv[3])+"/"+ // mass
                                std::string(argv[4])+"/"+ // energy
                                std::string(argv[5])+"/"; // ang

    double value, error;
    double t0, theta, phi, rc;
    size_t n_shower=0;
    Reconstruct reconstructData;
    for (size_t i=0; i < 1000; i++)
    {
        std::cout << "shower: " << i << '\n';
        std::string file_name =  std::string(argv[2])+"-"+ // model
                                 std::string(argv[3])+"-"+ // mass
                                 std::string(argv[4])+"-"+ // energy
                                 std::string(argv[5])+"-"+ // amg;
                                 shower_name(i)+".rec";

        
        if (exists_file(file_dir+file_name)){
            n_shower++;

            Array array(file_dir, file_name);
            // array.GenerateTimeCurvature(60,0);
            reconstructData.SetArray(array);
            reconstructData.ReconstructNmu();
            reconstructData.ReconstructAng();
            reconstructData.ClearArray();

           // Analysis analysisData(shower_dir+shower_name(i));
           // analysisData.AnalysisNmu();
            
            hist_n19.Fill(reconstructData.GetNmu());
          //  hist_nmu.Fill(analysisData.GetNmu());
          //  diff_nmu.Fill((reconstructData.GetNmu()-analysisData.GetNmu())/reconstructData.GetNmu());

            hist_t0.Fill(reconstructData.GetT0());
            hist_th.Fill(reconstructData.GetTheta());
            hist_ph.Fill(reconstructData.GetPhi());
            hist_rc.Fill(reconstructData.GetRc());
            
        }
 
    }
    hist_n19.Save("../../Data/ReconstructModel/n19.hist");
    hist_nmu.Save("../../Data/ReconstructModel/nmu.hist");
    diff_nmu.Save("../../Data/ReconstructModel/diff-nmu.hist");
    hist_t0.Save("../../Data/ReconstructModel/t0.hist");
    hist_th.Save("../../Data/ReconstructModel/th.hist");
    hist_ph.Save("../../Data/ReconstructModel/ph.hist");
    hist_rc.Save("../../Data/ReconstructModel/rc.hist");

    std::fstream file;
    std::string str_error;
    size_t nPos;
    file.open("../../Data/ReconstructModel/n19.hist", std::fstream::out | std::fstream::in | std::fstream::app);
    str_error = std::to_string(hist_n19.GetRMS());
    nPos = str_error.find_first_not_of("0.");
    file << std::fixed;
    file << "$[$ " << argv[2] << " $]$ "
              << "$\\langle N_{19} \\rangle = "
              << std::setprecision(2) << hist_n19.GetMean() << " \\pm " 
              << std::setprecision(2) << hist_n19.GetRMS() << "_{RMS} $" << '\n'; 
    file.close(); file.clear();
    str_error = std::to_string(hist_nmu.GetRMS());
    nPos= str_error.find_first_not_of("0.");
    file << std::fixed;
    file.open("../../Data/ReconstructModel/nmu.hist", std::fstream::out | std::fstream::in | std::fstream::app);
    file << "$[$ " << argv[2] << " $]$ "
              << "$\\langle N_{\\mu} \\rangle = "
              << std::setprecision(2) << hist_nmu.GetMean() << " \\pm " 
              << std::setprecision(2) << hist_nmu.GetRMS() << "_{RMS} $" << '\n';
    file.close(); file.clear();
    str_error = std::to_string(hist_t0.GetRMS());
    nPos = str_error.find_first_not_of("0.");
    file << std::fixed;
    file.open("../../Data/ReconstructModel/t0.hist", std::fstream::out | std::fstream::in | std::fstream::app);
    file << "$[$ " << argv[2] << " $]$ "
              << "$\\langle T_{0} \\rangle = "
              << std::setprecision(2) << hist_t0.GetMean() << " \\pm " 
              << std::setprecision(2) << hist_t0.GetRMS() << "_{RMS} $" << '\n';
    file.close(); file.clear();
    str_error = std::to_string(hist_th.GetRMS());
    nPos = str_error.find_first_not_of("0.");
    file << std::fixed;
    file.open("../../Data/ReconstructModel/th.hist", std::fstream::out | std::fstream::in | std::fstream::app);
    file << "$[$ " << argv[2] << " $]$ "
            << "$\\langle \\theta \\rangle = "
            << std::setprecision(2) << hist_th.GetMean() << " \\pm " 
            << std::setprecision(2) << hist_th.GetRMS() << "_{RMS} $" << '\n';
    file.close(); file.clear();
    str_error = std::to_string(hist_ph.GetRMS());
    nPos = str_error.find_first_not_of("0.");
    file << std::fixed;
    file.open("../../Data/ReconstructModel/ph.hist", std::fstream::out | std::fstream::in | std::fstream::app);
    file << "$[$ " << argv[2] << " $]$ "
            << "$\\langle \\phi \\rangle = "
            << std::setprecision(2) << hist_ph.GetMean() << " \\pm " 
            << std::setprecision(2) << hist_ph.GetRMS() << "_{RMS} $" << '\n';
    file.close(); file.clear();
    str_error = std::to_string(hist_rc.GetRMS());
    nPos = str_error.find_first_not_of("0.");
    file << std::fixed;
    file.open("../../Data/ReconstructModel/rc.hist", std::fstream::out | std::fstream::in | std::fstream::app);
    file << "$[$ " << argv[2] << " $]$ "
            << "$\\langle R_{c} \\rangle = "
            << std::setprecision(2) << hist_rc.GetMean() << " \\pm " 
            << std::setprecision(2) << hist_rc.GetRMS() << "_{RMS} $" << '\n';
    file.close(); file.clear();
    std::cout << "shower: " << n_shower << '\n';

    return 0;
}

inline bool exists_file (const std::string& name) {
    ifstream f(name.c_str());
    return f.good();
}

std::string shower_name (size_t i)
{
    std::string file_name;
    if (i < 10) 
        {
            file_name = "DAT00000" + std::to_string(i);
        }
        else if (i < 100 ) 
        {
            file_name = "DAT0000" + std::to_string(i);
        }
        else if (i < 1000 ) 
        {
            file_name = "DAT000" + std::to_string(i);
        }
        else 
        {
            file_name = "DAT00" + std::to_string(i);
        }
    return file_name;
}
