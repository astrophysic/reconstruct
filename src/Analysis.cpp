#include "Analysis.hpp"
#include "CorsikaBinary.hpp"

Analysis::Analysis(std::string fileName)
{
    this->fileName = fileName;
    this->nmu=0;
}

void Analysis::AnalysisNmu()
{
    CorsikaBinary shower(fileName);
    while (shower.NextShower())
    {
       ParticleArray particles = shower.GetParticles(); 
        for(auto particle : particles)
        {
            int type = particle->GetType();
            
            if (type == 5 || type == 6)
            {
                this->nmu+=particle->GetThinning();
            }
        }
    }
    this->nmu/=28229046;
}

double Analysis::GetNmu()
{
    return this->nmu;
}